// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyBCYG4GkfTIsVH6I63ZfQ1f-p775Nq79aI",
    authDomain: "ingreso-egreso-app-a63b5.firebaseapp.com",
    databaseURL: "https://ingreso-egreso-app-a63b5.firebaseio.com",
    projectId: "ingreso-egreso-app-a63b5",
    storageBucket: "ingreso-egreso-app-a63b5.appspot.com",
    messagingSenderId: "678351182798",
    appId: "1:678351182798:web:8741d74c3b01a9b29a299f",
    measurementId: "G-SM1N18V5CL",
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
