import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { LogingComponent } from "./components/auth/loging/loging.component";
import { DasboardComponent } from "./components/dashboard/dasboard/dasboard.component";
import { IngresoEgresoComponent } from "./components/ingreso-egreso/ingreso-egreso/ingreso-egreso.component";
import { EstadisticaComponent } from "./components/ingreso-egreso/estadistica/estadistica.component";
import { DetalleComponent } from "./components/ingreso-egreso/detalle/detalle.component";
import { FooterComponent } from "./components/shared/footer/footer.component";
import { NavbarComponent } from "./components/shared/navbar/navbar.component";
import { SidebarComponent } from "./components/shared/sidebar/sidebar.component";
import { RegisterComponent } from "./components/register/register/register.component";

import { AngularFireModule } from "@angular/fire";
import { AngularFirestoreModule } from "@angular/fire/firestore";
import { environment } from "src/environments/environment";
import { AngularFireAuthModule } from "@angular/fire/auth";

import { ReactiveFormsModule, FormsModule } from "@angular/forms";

@NgModule({
  declarations: [
    AppComponent,
    LogingComponent,
    DasboardComponent,
    IngresoEgresoComponent,
    EstadisticaComponent,
    DetalleComponent,
    FooterComponent,
    NavbarComponent,
    SidebarComponent,
    RegisterComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireAuthModule,
  ],
  providers: [],
  exports: [ReactiveFormsModule],
  bootstrap: [AppComponent],
})
export class AppModule {}
