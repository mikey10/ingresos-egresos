import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { LogingComponent } from "./components/auth/loging/loging.component";
import { RegisterComponent } from "./components/register/register/register.component";
import { DasboardComponent } from "./components/dashboard/dasboard/dasboard.component";
import { dashboardRoutes } from "./components/dashboard/dasboard.routes";
import { AuthGuard } from "./services/auth.guard";

const routes: Routes = [
  { path: "loging", component: LogingComponent },
  { path: "register", component: RegisterComponent },
  {
    path: "",
    component: DasboardComponent,
    children: dashboardRoutes,
    canActivate: [AuthGuard],
  },
  { path: "**", redirectTo: "" },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
