import { Injectable } from "@angular/core";
import { AngularFireAuth } from "@angular/fire/auth";
import { map } from "rxjs/operators";
import { Usuario } from "../models/Usuario";
import { AngularFirestore } from "@angular/fire/firestore";

@Injectable({
  providedIn: "root",
})
export class AuthService {
  constructor(
    public auth: AngularFireAuth,
    public firestore: AngularFirestore
  ) {}

  initAuthListener() {
    this.auth.authState.subscribe((fuser) => {
      console.log("email", fuser.email);
      console.log("udi", fuser.uid);
    });
  }

  CrearUsuario(nombre: string, email: string, password: string) {
    console.log("nombre:", nombre, "email:", email, "password:", password);
    return this.auth.auth
      .createUserWithEmailAndPassword(email, password)
      .then(({ user }) => {
        const newUser = new Usuario(user.uid, nombre, user.email);

        return this.firestore.doc(`${user.uid}/usuario`).set({ ...newUser });
      });
  }

  logueoUsuario(email: string, password: string) {
    console.log("logueo email:", email, "logueo password:", password);
    return this.auth.auth.signInWithEmailAndPassword(email, password);
  }

  isAuth() {
    return this.auth.authState.pipe(map((fbuser) => fbuser != null));
  }

  logOut() {
    return this.auth.auth.signOut();
  }
}
