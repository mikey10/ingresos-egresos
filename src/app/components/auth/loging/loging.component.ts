import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { AuthService } from "../../../services/auth.service";
import { Router } from "@angular/router";
// ES6 Modules or TypeScript
import Swal from "sweetalert2";

@Component({
  selector: "app-loging",
  templateUrl: "./loging.component.html",
  styleUrls: ["./loging.component.scss"],
})
export class LogingComponent implements OnInit {
  myLoging: FormGroup;

  constructor(
    private fb: FormBuilder,
    private serviceAut: AuthService,
    public router: Router
  ) {}

  ngOnInit() {
    this.myLoging = this.fb.group({
      correo: ["", Validators.required],
      password: ["", Validators.required],
    });
  }

  loginUser() {
    if (this.myLoging.invalid) {
      return;
    }
    console.log(this.myLoging);
    const { correo, password } = this.myLoging.value;
    this.serviceAut
      .logueoUsuario(correo, password)
      .then((res) => {
        console.log("logueo res:", res);
        this.router.navigate(["/"]);
      })
      .catch((error) => {
        console.log("logueo error", error);
        Swal.fire({
          title: "Error!",
          text: error.message,
          icon: "error",
          confirmButtonText: "Cool",
        });
      });
  }
}
