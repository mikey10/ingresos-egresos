import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { AuthService } from "../../../services/auth.service";
import { Router } from "@angular/router";
import Swal from "sweetalert2";

@Component({
  selector: "app-register",
  templateUrl: "./register.component.html",
  styleUrls: ["./register.component.scss"],
})
export class RegisterComponent implements OnInit {
  myForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private serviceAut: AuthService,
    public router: Router
  ) {}

  ngOnInit() {
    this.myForm = this.fb.group({
      nombre: ["", Validators.required],
      correo: ["", Validators.required],
      password: ["", Validators.required],
    });
  }

  registerUser() {
    console.log(this.myForm);

    Swal.fire({
      title: "Espere Por favor!",
      onBeforeOpen: () => {
        Swal.showLoading();
      },
    });
    /**Destructurin de valores */
    const { nombre, correo, password } = this.myForm.value;
    this.serviceAut
      .CrearUsuario(nombre, correo, password)
      .then((credenciales) => {
        console.log("credenciales", credenciales);
        Swal.close();
        this.router.navigate(["/"]);
      })
      .catch((error) => {
        Swal.close();
        console.log("error", error);
        Swal.fire({
          title: "Error!",
          text: error.message,
          icon: "error",
          confirmButtonText: "Cool",
        });
      });
  }
}
